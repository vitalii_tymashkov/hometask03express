const { dbAdapter } = require('../config/db');
const { v4 } = require('uuid');


class BaseRepository {
    constructor(collectionName) {
        this.dbContext = dbAdapter.get(collectionName);
        this.collectionName = collectionName;
    }

    generateId() {
        return v4();
    }

    getAll() {
        return this.dbContext.value();
    }

    getOne(search) {
        return this.dbContext.find(search).value();
    }

    getOneById(search) {
        return this.dbContext.find(element => element.id === search).value();
    }

    create(data) {
        data.id = this.generateId();
        if (data.health == undefined) {
            data.health = 100;
        }
        // data.createdAt = new Date();
        const list = this.dbContext.push(data).write();
        return list.find(it => it.id === data.id);
    }

    createUser(data) {
        const userFields = ['email', 'firstName', 'lastName', 'phoneNumber', 'password'];
        const entries = userFields.map(field => [field, data[field]]);
        const userData = Object.fromEntries(entries)
        userData.id = this.generateId();
        userData.createdAt = new Date();

        const list = this.dbContext.push(userData).write();
        return list.find(it => it.id === userData.id);
    }

    createFighter(data) {
        const fighterFields = ['name', 'power', 'defense', 'health'];
        const entries = fighterFields.map(field => [field, data[field]]);
        const fighterData = Object.fromEntries(entries)
        fighterData.id = this.generateId();
        fighterData.createdAt = new Date();

        if (fighterData.health == undefined) {
            fighterData.health = 100;
        }

        const list = this.dbContext.push(fighterData).write();
        return list.find(it => it.id === fighterData.id);
    }


    update(id, dataToUpdate) {
        dataToUpdate.updatedAt = new Date();
        return this.dbContext.find({ id }).assign(dataToUpdate).write();
    }

    updateUser(id, dataToUpdate) {
        const userFields = ['email', 'firstName', 'lastName', 'phoneNumber', 'password'];
        const entries = userFields.map(field => [field, dataToUpdate[field]]);
        const userData = Object.fromEntries(entries)

        userData.updatedAt = new Date();
        return this.dbContext.find({ id }).assign(userData).write();
    }

    updateFighter(id, dataToUpdate) {

        const fighterFields = ['name', 'power', 'defense', 'health'];
        const entries = fighterFields.map(field => [field, dataToUpdate[field]]);
        const fighterData = Object.fromEntries(entries)


        if (fighterData.health == undefined) {
            fighterData.health = 100;
        }

        fighterData.updatedAt = new Date();
        return this.dbContext.find({ id }).assign(fighterData).write();
    }

    delete(id) {
        return this.dbContext.remove(element => element.id === id).write();
    }
}

exports.BaseRepository = BaseRepository;
